import sublime, sublime_plugin,re,sys

class FormatDetectCommand(sublime_plugin.EventListener):

	defRuler = []

	def sysout(msg, end='\r'):
		sys.stdout.write(str(msg) + end)


	def detectFormat(self, view, threshold):
		if view.settings().get('syntax').rpartition('/')[0] != 'Packages/COBOL Syntax':
				view.settings().set("ruler", self.defRuler)
				return
        	
		sample = view.substr(sublime.Region(0, min(view.size(), 2**14)))

		for line in sample.split("\n"):
			if not line: continue

			line = line.upper()

			if ">SOURCE FORMAT IS FIXED" in line :
				view.settings().set("rulers", [7,11,12,72])
				return

			if ">SOURCE FORMAT IS VARIABLE" in line :
				view.settings().set("rulers", [7,11,12])
				return

			if "SOURCEFORMAT\"FIXED\"" in line :
				view.settings().set("rulers", [7,11,12,72])
				return

			if ">SOURCE FORMAT IS VARIABLE" in line:
				view.settings().set("rulers", [7,11,12])
				return

			if "SOURCEFORMAT\"VARIABLE\"" in line :
				view.settings().set("rulers", [7,11,12])
				return

			if "SOURCEFORMAT\"FREE\"" in line :
				view.settings().set("rulers", [])
				return

		view.settings().set("rulers", self.defRuler)


	def on_load(self, view):
		self.defRuler = view.settings().get("rulers")
		self.detectFormat(view, 10)

	def run(self, view, args):
		self.defRuler = view.settings().get("rulers")
		self.detectFormat(view, 1)			
