set -x
python json2plist.py jcl.tmLanguage.json
[ $? -ne 0 ] && echo "Failed" && exit 1
python json2plist.py OpenCOBOL.tmLanguage.json
[ $? -ne 0 ] && echo "Failed" && exit 1
python json2plist.py COBOL.tmLanguage.json
[ $? -ne 0 ] && echo "Failed" && exit 1
python json2plist.py ACUCOBOL.tmLanguage.json
[ $? -ne 0 ] && echo "Failed" && exit 1
python json2plist.py Comments.tmPreferences.json
[ $? -ne 0 ] && echo "Failed" && exit 1
python json2plist.py dir.tmLanguage.json
[ $? -ne 0 ] && echo "Failed" && exit 1
python json2plist.py mfu.tmLanguage.json
[ $? -ne 0 ] && echo "Failed" && exit 1
python json2plist.py utreport.tmLanguage.json
[ $? -ne 0 ] && echo "Failed" && exit 1

rm -f ../Support/*language
cp jcl.json.plist ../jcl.tmLanguage

cp COBOL.tmLanguage.json.plist ../COBOL.tmLanguage

cp OpenCOBOL.tmLanguage.json.plist ../OpenCOBOL.tmLanguage

cp ACUCOBOL.tmLanguage.json.plist ../ACUCOBOL.tmLanguage

cp Comments.tmPreferences.json.plist ../Comments.tmPreferences

cp dir.tmLanguage.json.plist ../dir.tmLanguage

cp mfu.tmLanguage.json.plist ../mfu.tmLanguage

cp utreport.tmLanguage.json.plist ../utreport.tmLanguage

rm *.plist
cd ..
rm  "../COBOL Syntax.sublime-package"
zip -q "../COBOL Syntax.sublime-package" -r *
# cp "../COBOL Syntax.sublime-package" "/Users/spg/Library/Application Support/Sublime Text 3/Installed Packages/"
