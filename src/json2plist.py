
import sys
import json
from plistlib import writePlist

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

if __name__ == '__main__':
    filename = sys.argv[1]
    with open(filename, 'r') as f:
        data = json.load(StringIO(f.read()))
    with open(filename + '.plist', 'wb') as plist_file:
        writePlist(data, plist_file)