
setlocal
set PATH=c:\tools;%PATH%
cd src
for %%i in (*.json) do python json2plist.py %%i
copy ACUCOBOL.tmLanguage.json.plist ..\ACUCOBOL.tmLanguage
copy COBOL.tmLanguage.json.plist ..\COBOL.tmLanguage
copy Comments.tmPreferences.json.plist ..\Comments.tmPreferences
copy jcl.tmLanguage.json.plist ..\jcl.tmLanguage
copy OpenCOBOL.tmLanguage.json.plist ..\OpenCOBOL.tmLanguage
del *.plist
cd ..
zip "..\COBOL Syntax.sublime-package" -r . -x src
copy "..\COBOL Syntax.sublime-package" "C:\tools\sublime3\Data\Installed Packages"